import React from "react";
import ReactDOM from "react-dom";
import MessageListView from "./views/MessageListView";

const NewApp = require("./views/MessageListView").default;

function renderApp(App) {
  ReactDOM.render(<App />, document.getElementById("root"));
}

renderApp(MessageListView);

if (module.hot) {
  module.hot.accept("./views/MessageListView", () => {
    renderApp(NewApp);
  });
}
