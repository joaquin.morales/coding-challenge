import React, { useReducer, useState, useEffect, useCallback } from "react";
import { initialState, reducer, ACTIONS } from "./prioritiesReducer";
import { NOTIFICATION_TYPES } from "./constants";
import useApi from "../hooks/useApi";

const initialValue = {
  clearAll: () => {},
  toggleGeneration: () => {},
  removeNotification: () => {},
  listsByPriorities: {},
  bannerNotification: {},
  isGenerating: false,
};
export const PrioritiesContext = React.createContext(initialValue);

export const PrioritiesProvider = ({ children }) => {
  const [listsByPriorities, dispatch] = useReducer(reducer, initialState);
  const [bannerNotification, setBannerNotification] = useState({});
  const {
    newNotification,
    toggle: toggleGeneration,
    isStarted: isGenerating,
  } = useApi();

  useEffect(() => {
    dispatch({ type: ACTIONS.addNotification, payload: newNotification });
    if (newNotification.priority === NOTIFICATION_TYPES.error) {
      setBannerNotification(newNotification);
    }
  }, [newNotification, setBannerNotification]);

  const clearAll = useCallback(() => dispatch({ type: ACTIONS.clearAll }), [
    dispatch,
  ]);
  const removeNotification = useCallback(
    (notification) => {
      dispatch({ type: ACTIONS.removeNotification, payload: notification });
    },
    [dispatch]
  );

  return (
    <PrioritiesContext.Provider
      value={{
        clearAll,
        toggleGeneration,
        removeNotification,
        listsByPriorities,
        isGenerating,
        bannerNotification,
      }}
    >
      {children}
    </PrioritiesContext.Provider>
  );
};
