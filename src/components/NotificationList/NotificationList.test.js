import React from "react";
import { shallow } from "enzyme";
import NotificationList from "./NotificationList";

describe("Notification List component", () => {
  it("should render with empty title and zero counter by default", () => {
    const cmpt = shallow(<NotificationList />);
    expect(
      cmpt.find("WithStyles(ForwardRef(Typography))").at(0).text()
    ).toEqual("");
    expect(
      cmpt.find("WithStyles(ForwardRef(Typography))").at(1).text()
    ).toEqual("Count 0");
  });

  it("should should count 2 and show 2 Notifications when 2 notifications are passed", () => {
    const notifications = [
      { id: "1", message: "1", priority: 1 },
      { id: "2", message: "2", priority: 1 },
    ];
    const cmpt = shallow(<NotificationList notifications={notifications} />);

    expect(
      cmpt.find("WithStyles(ForwardRef(Typography))").at(1).text()
    ).toEqual("Count 2");

    expect(cmpt.find("Notification")).toHaveLength(2);
  });
});
