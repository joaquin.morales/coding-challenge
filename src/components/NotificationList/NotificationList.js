import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Notification from "../Notification/Notification";

const useStyles = makeStyles({
  notificationList: {
    display: "flex",
    flexDirection: "column",
    padding: "15px",
    flexGrow: 1,
    flexShrink: 0,
    flexBasis: 0,
  },
  item: {
    margin: "5px 0px",
  },
});

const NotificationList = ({
  title = "",
  notifications = [],
  onNotificationClear,
}) => {
  const classes = useStyles();

  return (
    <Paper elevation={0} className={classes.notificationList}>
      <Typography variant="subtitle1">{title}</Typography>
      <Typography variant="subtitle2">{`Count ${notifications.length}`}</Typography>
      {notifications.map((notification) => (
        <Notification
          key={notification.id}
          className={classes.item}
          {...notification}
          onClear={onNotificationClear}
        />
      ))}
    </Paper>
  );
};

NotificationList.propTypes = {
  title: PropTypes.string,
  notifications: PropTypes.array,
  onNotificationClear: PropTypes.func,
};

export default NotificationList;
