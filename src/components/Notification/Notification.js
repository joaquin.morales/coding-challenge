import React, { useCallback } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { NOTIFICATION_TYPES, COLORS } from "../constants";

const useStyles = makeStyles({
  content: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "15px",
  },
  button: {
    textTransform: "none",
  },
});

const Notification = ({
  id,
  message = "",
  priority = NOTIFICATION_TYPES.info,
  className,
  onClear,
}) => {
  const classes = useStyles();

  const handleClear = useCallback(() => onClear({ id, priority }), [
    id,
    priority,
    onClear,
  ]);

  return (
    <Paper
      className={`${classes.content} ${className}`}
      style={{ backgroundColor: COLORS[priority] }}
    >
      <Typography variant="subtitle2">{message}</Typography>
      <Button size="small" onClick={handleClear} className={classes.button}>
        Clear
      </Button>
    </Paper>
  );
};

Notification.propTypes = {
  id: PropTypes.string,
  message: PropTypes.string,
  priority: PropTypes.oneOf(Object.values(NOTIFICATION_TYPES)),
  className: PropTypes.string,
  onClear: PropTypes.func,
};

export default Notification;
