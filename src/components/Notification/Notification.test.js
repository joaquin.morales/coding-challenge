import React from "react";
import { shallow } from "enzyme";
import Notification from "./Notification";

describe("Notification Component", () => {
  it("should render with empty message when no message is passed", () => {
    const cmpt = shallow(<Notification />);
    expect(cmpt.find("WithStyles(ForwardRef(Typography))").text()).toEqual("");
  });
  it("should render a meessage when it is passed", () => {
    const message = "test message";
    const cmpt = shallow(<Notification message={message} />);
    expect(cmpt.find("WithStyles(ForwardRef(Typography))").text()).toEqual(
      message
    );
  });
  it('should have a button with "clear" as text', () => {
    const cmpt = shallow(<Notification />);
    expect(cmpt.find("WithStyles(ForwardRef(Button))").text()).toEqual("Clear");
  });

  it("should call onClear function with id whe the button is clicked", () => {
    const handleClear = jest.fn();
    const cmpt = shallow(<Notification onClear={handleClear} />);
    cmpt.find("WithStyles(ForwardRef(Button))").simulate("click");
    expect(handleClear).toHaveBeenCalled();
  });
});
