import uniqueId from "lodash/uniqueId";
import { NOTIFICATION_TYPES } from "./constants";

export const ACTIONS = {
  addNotification: "add_notification",
  removeNotification: "remove_notification",
  clearAll: "clear_all",
};

export const initialState = {
  errorNotifications: [],
  warnNotifications: [],
  infoNotifications: [],
};
const priorityMapper = {
  [NOTIFICATION_TYPES.error]: "errorNotifications",
  [NOTIFICATION_TYPES.warning]: "warnNotifications",
  [NOTIFICATION_TYPES.info]: "infoNotifications",
};

export function reducer(state, action) {
  const { type, payload } = action;
  switch (type) {
    case ACTIONS.addNotification: {
      const listKey = priorityMapper[payload.priority];
      const id = uniqueId(`${listKey}_`);
      return { ...state, [listKey]: [{ ...payload, id }, ...state[listKey]] };
    }
    case ACTIONS.removeNotification: {
      const listKey = priorityMapper[payload.priority];
      const filteredList = state[listKey].filter(
        (notification) => notification.id !== payload.id
      );
      return { ...state, [listKey]: filteredList };
    }
    case ACTIONS.clearAll:
      return initialState;
    default:
      return state;
  }
}
