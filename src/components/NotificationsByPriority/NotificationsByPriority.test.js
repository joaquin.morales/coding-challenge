import React from "react";
import { shallow } from "enzyme";
import NotificationsByPriority from "./NotificationsByPriority";

describe("Notifications By Priority Component", () => {
  it("should render 3 notifications Lists", () => {
    const cmpt = shallow(<NotificationsByPriority />);
    expect(cmpt.find("NotificationList")).toHaveLength(3);
  });

  it("should show a title for each type of notification list", () => {
    const cmpt = shallow(<NotificationsByPriority />);
    const allLists = cmpt.find("NotificationList");
    expect(allLists.at(0).prop("title")).toEqual("Error Type 1");
    expect(allLists.at(1).prop("title")).toEqual("Warning Type 2");
    expect(allLists.at(2).prop("title")).toEqual("Info Type 3");
  });
});
