import React, { useContext } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { NOTIFICATION_TYPES } from "../constants";
import NotificationList from "../NotificationList/NotificationList";
import { PrioritiesContext } from "../PrioritiesContext";

const useStyles = makeStyles({
  container: {
    display: "flex",
  },
});

const PrioritiesList = [
  {
    id: "errorNotifications",
    title: "Error Type 1",
    priority: NOTIFICATION_TYPES.error,
  },
  {
    id: "warnNotifications",
    title: "Warning Type 2",
    priority: NOTIFICATION_TYPES.warning,
  },
  {
    id: "infoNotifications",
    title: "Info Type 3",
    priority: NOTIFICATION_TYPES.info,
  },
];

const NotificationsByPriority = ({ className }) => {
  const { listsByPriorities, removeNotification } = useContext(
    PrioritiesContext
  );

  const classes = useStyles();
  return (
    <div className={`${classes.container} ${className}`}>
      {PrioritiesList.map(({ id, ...props }) => (
        <NotificationList
          key={id}
          {...props}
          notifications={listsByPriorities[id]}
          onNotificationClear={removeNotification}
        />
      ))}
    </div>
  );
};

NotificationsByPriority.propTypes = {
  className: PropTypes.string,
};

export default NotificationsByPriority;
