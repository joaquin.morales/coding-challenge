import React from "react";
import { shallow } from "enzyme";
import AlertMessage from "./AlertMessage";

describe("Alert Message component", () => {
  it("should render a meesage with a close button", () => {
    const cmpt = shallow(<AlertMessage message="test" />);
    expect(cmpt.find("WithStyles(ForwardRef(Typography))").text()).toEqual(
      "test"
    );
    expect(cmpt.find("WithStyles(ForwardRef(IconButton))")).toHaveLength(1);
  });

  it("should call onClose function when button is clicked", () => {
    const closeHandler = jest.fn();
    const cmpt = shallow(<AlertMessage onClose={closeHandler} />);
    cmpt.find("WithStyles(ForwardRef(IconButton))").simulate("click");
    expect(closeHandler).toHaveBeenCalled();
  });
});
