import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { COLORS, NOTIFICATION_TYPES } from "../constants";

const useStyles = makeStyles({
  alert: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: COLORS[NOTIFICATION_TYPES.error],
    width: "350px",
  },
  message: {
    padding: "12px 12px 12px 0px",
  },
  button: {
    margin: "0px 8px",
  },
});

const AlertMessage = ({ message = "", onClose }) => {
  const classes = useStyles();
  return (
    <Paper className={classes.alert}>
      <IconButton size="small" className={classes.button} onClick={onClose}>
        <CloseIcon fontSize="small" />
      </IconButton>
      <Typography variant="subtitle2" className={classes.message}>
        {message}
      </Typography>
    </Paper>
  );
};

AlertMessage.propTypes = {
  message: PropTypes.string,
  onClose: PropTypes.func,
};

export default AlertMessage;
