export const NOTIFICATION_TYPES = {
  error: 1,
  warning: 2,
  info: 3,
};

export const COLORS = {
  [NOTIFICATION_TYPES.error]: "#F56236",
  [NOTIFICATION_TYPES.warning]: "#FCE788",
  [NOTIFICATION_TYPES.info]: "#88FCA3",
};
