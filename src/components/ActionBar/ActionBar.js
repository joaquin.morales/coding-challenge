import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { PrioritiesContext } from "../PrioritiesContext";

const useStyles = makeStyles({
  bar: {
    display: "flex",
    justifyContent: "center",
    padding: "15px",
  },
  button: {
    backgroundColor: "#00e2c4",
    margin: "0px 5px",
  },
});

const ActionBar = () => {
  const classes = useStyles();
  const { toggleGeneration, clearAll, isGenerating } = useContext(
    PrioritiesContext
  );
  return (
    <Paper elevation={0} className={classes.bar}>
      <Button
        variant="contained"
        className={classes.button}
        onClick={toggleGeneration}
      >
        {isGenerating ? "STOP" : "START"}
      </Button>
      <Button variant="contained" className={classes.button} onClick={clearAll}>
        CLEAR
      </Button>
    </Paper>
  );
};

export default ActionBar;
