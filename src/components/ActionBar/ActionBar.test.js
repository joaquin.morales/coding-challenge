import React from "react";
import { shallow } from "enzyme";
import ActionBar from "./ActionBar";

describe("ActionBar component", () => {
  it("should have two buttons for start and clear actions", () => {
    const cmpt = shallow(<ActionBar />);
    const arrayButtons = cmpt.find("WithStyles(ForwardRef(Button))");
    expect(arrayButtons).toHaveLength(2);
    expect(arrayButtons.at(0).text()).toEqual("START");
    expect(arrayButtons.at(1).text()).toEqual("CLEAR");
  });
});
