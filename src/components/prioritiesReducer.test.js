import { reducer, ACTIONS, initialState } from "./prioritiesReducer";

describe("reducer functionality", () => {
  it("should leave the state untoched when action does not matck", () => {
    const newState = reducer(initialState, {
      type: "unknown_action",
    });
    expect(newState).toEqual(initialState);
  });

  it("should add notification to error list when notification is priority 1", () => {
    const notification = { id: 1, priority: 1, message: "error" };
    const newState = reducer(initialState, {
      type: ACTIONS.addNotification,
      payload: notification,
    });
    expect(newState.errorNotifications).toHaveLength(1);
  });

  it("should add notification to info and the rest list should remain untoched", () => {
    const notification = { id: 1, priority: 3, message: "error" };
    const newState = reducer(initialState, {
      type: ACTIONS.addNotification,
      payload: notification,
    });
    expect(newState.infoNotifications).toHaveLength(1);
    expect(newState.errorNotifications).toHaveLength(0);
    expect(newState.warnNotifications).toHaveLength(0);
  });

  describe("remove functionality and clear", () => {
    let currentState = {};
    beforeEach(() => {
      currentState = {
        errorNotifications: [
          { id: 1, priority: 1 },
          { id: 2, priority: 1 },
        ],
        warnNotifications: [{ id: 3, priority: 2 }],
        infoNotifications: [
          { id: 4, priority: 3 },
          { id: 5, priority: 3 },
        ],
      };
    });

    it("should remove from warnNotifications when priority is 2 and id match", () => {
      const newState = reducer(currentState, {
        type: ACTIONS.removeNotification,
        payload: { id: 3, priority: 2 },
      });
      expect(newState.warnNotifications).toHaveLength(0);
      expect(newState.errorNotifications).toHaveLength(2);
      expect(newState.infoNotifications).toHaveLength(2);
    });

    it("should remain the same when id does not match at remove", () => {
      const newState = reducer(currentState, {
        type: ACTIONS.removeNotification,
        payload: { id: 100, priority: 3 },
      });
      expect(newState.errorNotifications).toHaveLength(2);
      expect(newState.warnNotifications).toHaveLength(1);
      expect(newState.infoNotifications).toHaveLength(2);
    });

    it("should empty all lists when clear action is is called", () => {
      const newState = reducer(currentState, {
        type: ACTIONS.clearAll,
      });
      expect(newState.errorNotifications).toHaveLength(0);
      expect(newState.warnNotifications).toHaveLength(0);
      expect(newState.infoNotifications).toHaveLength(0);
    });
  });
});
