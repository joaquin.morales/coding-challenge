import React from "react";
import { shallow } from "enzyme";
import SnackNotification from "./SnackNotification";

describe("Snack Notification component", () => {
  it("should render a Snack component with AlertMessage type", () => {
    const cmpt = shallow(<SnackNotification />);
    expect(cmpt.find("WithStyles(ForwardRef(Snackbar))")).toHaveLength(1);
    expect(cmpt.find("AlertMessage")).toHaveLength(1);
  });

  it("should hide after 2 seconds", () => {
    const cmpt = shallow(<SnackNotification />);
    expect(
      cmpt.find("WithStyles(ForwardRef(Snackbar))").prop("autoHideDuration")
    ).toEqual(2000);
  });
});
