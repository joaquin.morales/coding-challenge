import React, { useContext, useState, useEffect, useCallback } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
import { PrioritiesContext } from "../PrioritiesContext";
import AlertMessage from "../AlertMessage/AlertMessage";

const useStyles = makeStyles({
  root: {
    top: "5px",
  },
});

const SnackNotification = () => {
  const classes = useStyles();
  const { bannerNotification } = useContext(PrioritiesContext);
  const [snackProps, setSnackProps] = useState({
    open: false,
    message: "",
  });

  useEffect(() => {
    const { message } = bannerNotification;
    if (message) {
      setSnackProps({ open: true, message });
    }
  }, [bannerNotification]);

  const handleClose = useCallback(
    (_, reason) => {
      if (reason && reason !== "timeout") {
        return;
      }
      setSnackProps((props) => ({ ...props, open: false }));
    },
    [setSnackProps]
  );

  return (
    <Snackbar
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      className={classes.root}
      onClose={handleClose}
      autoHideDuration={2000}
      open={snackProps.open}
    >
      <AlertMessage message={snackProps.message} onClose={handleClose} />
    </Snackbar>
  );
};

export default SnackNotification;
