import React from "react";
import Divider from "@material-ui/core/Divider";
import {
  NotificationsByPriority,
  ActionBar,
  SnackNotification,
} from "../components";
import { PrioritiesProvider } from "../components/PrioritiesContext";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    marginTop: "60px",
  },
  body: {
    marginTop: "40px",
  },
});

const MessageListView = () => {
  const classes = useStyles();
  return (
    <PrioritiesProvider>
      <div className={classes.container}>
        <SnackNotification />
        <Divider />
        <ActionBar />
        <NotificationsByPriority className={classes.body} />
      </div>
    </PrioritiesProvider>
  );
};

export default MessageListView;
