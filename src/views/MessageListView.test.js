import React from "react";
import { shallow } from "enzyme";
import MessageListView from "./MessageListView";

describe("Main List view", () => {
  it("should render with a top banner section, an action bar and notifications by priority", () => {
    const cmpt = shallow(<MessageListView />);
    expect(cmpt.find("SnackNotification")).toHaveLength(1);
    expect(cmpt.find("ActionBar")).toHaveLength(1);
    expect(cmpt.find("NotificationsByPriority")).toHaveLength(1);
  });
});
