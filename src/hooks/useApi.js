import { useEffect, useCallback, useReducer } from "react";
import Chance from "chance";
import lodash from "lodash";

const chance = new Chance();

const generateNewNotification = () => ({
  message: chance.string(),
  priority: lodash.random(1, 3),
});

const initialState = {
  generating: true,
  newNotification: generateNewNotification(),
};

export const ACTIONS = {
  startGenerating: "start_generating",
  stopGenerating: "stop_generating",
  toggleGeneration: "toggle_generation",
  generateNewNotification: "generate_new_notification",
};

const useApi = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const nextInMS = lodash.random(500, 3000);
    setTimeout(
      () => dispatch({ type: ACTIONS.generateNewNotification }),
      nextInMS
    );
  }, [state.newNotification]);

  const start = useCallback(
    () => dispatch({ type: ACTIONS.startGenerating }),
    []
  );
  const stop = useCallback(
    () => dispatch({ type: ACTIONS.stopGenerating }),
    []
  );

  const toggle = useCallback(() => {
    if (state.generating) {
      stop();
      return;
    }
    start();
  }, [state.generating, start, stop]);

  return {
    start,
    stop,
    toggle,
    isStarted: state.generating,
    newNotification: state.newNotification,
  };
};

export function reducer(state, action) {
  switch (action.type) {
    case ACTIONS.startGenerating:
      return { generating: true, newNotification: generateNewNotification() };
    case ACTIONS.stopGenerating:
      return { ...state, generating: false };
    case ACTIONS.generateNewNotification:
      return state.generating
        ? { ...state, newNotification: generateNewNotification() }
        : state;
    default:
      return state;
  }
}

export default useApi;
