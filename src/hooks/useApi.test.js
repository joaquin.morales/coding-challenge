import React from "react";
import useApi, { ACTIONS, reducer } from "./useApi";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";

describe("useApi reducer", () => {
  it("should generate a new message as soon as the start is hit", () => {
    const initialState = { generating: false, newNotification: {} };
    const newState = reducer(initialState, { type: ACTIONS.startGenerating });
    expect(newState.generating).toBe(true);
    expect(isEmpty(newState.newNotification)).toBe(false);
  });

  it("should stop generating when stop action is hit", () => {
    const initialState = {
      generating: true,
      newNotification: { message: "", priority: 1 },
    };
    const newState = reducer(initialState, { type: ACTIONS.stopGenerating });
    expect(newState.generating).toBe(false);
  });

  it("should generate a new message while generating flag is true and generate new message is hit", () => {
    const initialState = { generating: true, newNotification: {} };
    const newState = reducer(initialState, {
      type: ACTIONS.generateNewNotification,
    });
    expect(isEmpty(newState.newNotification)).toBe(false);
  });

  it("should not generate a new message when generating flag is off even if the action is asked", () => {
    const initialState = { generating: false, newNotification: {} };
    const newState = reducer(initialState, {
      type: ACTIONS.generateNewNotification,
    });
    expect(isEmpty(newState.newNotification)).toBe(true);
  });

  it("should remain unchanged the state when the action required does not match any predefined action", () => {
    const initialState = {
      generating: true,
      newNotification: { message: "", priority: 1 },
    };
    const newState = reducer(initialState, { type: "unknown_action" });
    expect(isEqual(initialState, newState)).toBe(true);
  });
});
